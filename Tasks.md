#Tasks

1. Create a fork of https://bitbucket.org/idreaminteractive/idreamjstesting
2. All testing and playing can be done on a local copy. No webserver is needed.
3. Fix JS errors and loading errors that come up when the index.html is loaded.
4. Make single tap clear eggs
5. Make HighScore button - make JS alert window stating: "Feature coming soon!"
6. When a user loses, make the submit button on post game menu open to http://www.idreaminteractive.com/ in a new tab, not attempt to go to Facebook.
7. All eggs drop at same speed at the moment. Make them drop at random speeds, scaling with the level the user is on. In addition, since we only have to tap once to clear it they should be faster overall than the current version.
8. Add two more random messages that can appear after a user loses on a level. Use whatever you'd like for the messages.
9. Once complete, create a pull request against master to submit the changes.
